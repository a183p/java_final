import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javaFlacEncoder.FLAC_FileEncoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class request {

    String req_string;
    String file_path;

    public request(String req, String file) throws IOException, MalformedURLException, JSONException {
        req_string = req;
        file_path = file;
        soundconvert();
        connect2google();
    }

    private void connect2google() throws MalformedURLException, IOException, JSONException {
        URL url = new URL(req_string);
        String USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        ///header
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Content-Type", "audio/x-flac;rate=44100");
        connection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.write(Files.readAllBytes(Paths.get(file_path + ".flac")));

        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();
        System.out.println("Sending 'POST' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                connection.getInputStream()));
        String inputLine;
        String response = "";
        inputLine = in.readLine();
        while ((inputLine = in.readLine()) != null) {
            response += inputLine;
        }
        in.close();
        connection.disconnect();
        //System.out.println(response);
        System.out.println(response);
        String output = tran_string(response);
        System.out.println(output);
        output_string op = new output_string(output);
    }

    private void soundconvert() {
        FLAC_FileEncoder flacEncoder = new FLAC_FileEncoder();
        File inputFile = new File(file_path + ".wav");
        File outputFile = new File(file_path + ".flac");
        flacEncoder.encode(inputFile, outputFile);
        System.out.println("convert Done");
    }

    private String tran_string(String input) throws JSONException {
        String result = "";
        //System.out.println("js string" + input);
        JSONObject obj = new JSONObject(input);
        JSONObject res = obj.getJSONArray("result").getJSONObject(0);
        JSONArray arr = res.getJSONArray("alternative");
        JSONObject best = arr.getJSONObject(0);
        String output = best.getString("transcript");
        result = output;
        return result;
    }
}
