
import java.io.*;
import javax.sound.sampled.*;

public class AudioTest extends Thread {

    // 產生TargetDataLine類別的變數m_targetdataline
    static TargetDataLine m_targetdataline;

    // 透過TargetDataLine介面(繼承自DataLine)與音效卡溝通 target目標
    // 產生AudioFileFormat.Type類別的變數m_targetType Format格式
    static AudioFileFormat.Type m_targetType;

    // 產生AudioInputStream類別的變數m_audioInputStream stream流
    static AudioInputStream m_audioInputStream;

    static File m_outputFile;// 產生File類別的變數 m_outputFile
    static boolean m_bRecording;// 後面需用到布林函數 True,False

    public AudioTest(TargetDataLine line, AudioFileFormat.Type targetType, File file) {
        m_targetdataline = line;
        m_audioInputStream = new AudioInputStream(line);
        m_targetType = targetType;
        m_outputFile = file;
    }

    public void start() {
        m_targetdataline.start();
        super.start();
    }

    public void stopRecording() {

        m_targetdataline.stop();
        m_targetdataline.close();
        m_bRecording = false;
    }

    public void run() {
        try {
            AudioSystem.write(m_audioInputStream, m_targetType, m_outputFile);
            System.out.println("after write()");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
