/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import org.jnativehook.keyboard.NativeKeyEvent;

public class output_string implements ClipboardOwner {

    Robot rb = null;
    private Clipboard clipboard;

    public output_string(String input) {
        try {
            rb = new Robot();
        } catch (AWTException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        setBookContents(input);
        outputstring();
    }

    private void setBookContents(String str) {
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection contents = new StringSelection(str);
        clipboard.setContents(contents, this);
    }

    private void outputstring() {
        rb.keyPress(NativeKeyEvent.VK_CONTROL);
        rb.keyPress(NativeKeyEvent.VK_V);
        rb.keyRelease(NativeKeyEvent.VK_CONTROL);
        rb.keyRelease(NativeKeyEvent.VK_V);
    }

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }

}
